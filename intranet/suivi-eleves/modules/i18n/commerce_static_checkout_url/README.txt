This module changes the default functionality of Commerce to use a static url
in checkout instead of dynamic one that changes in every order.
It is supposed to be a helper module for banking institutes
that don't accept dynamic urls.

It simplifies development of redirection-payment modules, by allowing developer
to deal only with each bank and not with the static url needed.

All Greek banks that offer a redirection payment method require static urls to
work. This is where commerce_static_checkout_url comes in, it creates the
static url so the bank can recognize your site.

Modules for the greek banks Eurobank and Alphabank are already created by the
authors of this module while a module for Winbank (Peiraius Bank) is under
development but there is no timeline on its development.

Development
This module is developed by 1024.gr and SRM.gr
The developers can create modules for other banks on demand.
This can be done for any bank that uses the redirection method.

Sponsorship
Development was sponsored by SRM.gr
